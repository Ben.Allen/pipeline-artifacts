package main

import (
	"archive/zip"
	"bytes"
	"flag"
	"io"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/xanzy/go-gitlab"
)

// How I test:
// CI_JOB_TOKEN=${GFS_GITLAB_TOKEN} CI_PROJECT_NAMESPACE=gfs/products/technology/upf/sandbox go run main.go 1091535928

var (
	token   = os.Getenv("CI_JOB_TOKEN")
	project = os.Getenv("CI_PROJECT_NAMESPACE")
	dest    = flag.String("dest", "./tmp/", "destination directory for artifacts")
)

func main() {
	flag.Parse()

	target, err := filepath.Abs(filepath.Clean(*dest))
	if err != nil {
		log.Fatal(err)
	}

	*dest = target + "/"
	log.Println("Using target path: " + *dest)

	for _, arg := range flag.Args() {
		if id, err := strconv.Atoi(arg); err == nil {
			err := GetPipelineArtifacts(*dest, id)
			if err != nil {
				log.Fatal(err)
			}
		} else {
			log.Println(arg + " must be an int")
		}
	}
}

func GetPipelineArtifacts(dest string, id int) error {
	// Setup Gitlab Client
	git, err := gitlab.NewClient(token)
	if err != nil {
		return err
	}

	// Get the Pipeline
	pipeline, _, err := git.Pipelines.GetPipeline(project, id)
	if err != nil {
		return err
	}

	log.Printf("Found pipeline: %v - %s", pipeline.ID, pipeline.Status)

	// Get Pipeline Bridges.  These are downstream pipelines
	bridges, _, err := git.Jobs.ListPipelineBridges(pipeline.ProjectID, pipeline.ID, &gitlab.ListJobsOptions{})
	if err != nil {
		return err
	}

	// Loop over all bridges
	for _, bridge := range bridges {
		log.Printf("  Found Bridge: %v - %v (%s) - %s", bridge.ID, bridge.DownstreamPipeline.ID, bridge.Name, bridge.Status)

		// Cascade and get all artifacts from downstream pipelines
		err := GetPipelineArtifacts(dest+bridge.Name+"/", bridge.DownstreamPipeline.ID)
		if err != nil {
			log.Fatal(err)
		}
	}

	// List all jobs in the pipeline
	jobs, _, err := git.Jobs.ListPipelineJobs(pipeline.ProjectID, pipeline.ID, &gitlab.ListJobsOptions{})
	if err != nil {
		return err
	}

	// Loop over all jobs
	for _, job := range jobs {
		log.Printf("  Found Job: %v (%s) - %s", job.ID, job.Name, job.Status)

		// Range over artifacts
		for _, artifact := range job.Artifacts {
			log.Printf("    Found Artifact: %s: %d", artifact.FileType, artifact.Size)

			// Check if there is an artifact of type archive to limit API calls when a job has no artifacts.
			if artifact.FileType == "archive" {
				// Get a byte stream of the artifacts zip for this job
				reader, res, err := git.Jobs.GetJobArtifacts(pipeline.ProjectID, job.ID)
				if err != nil {
					// Returns error if there are no artifacts.  Just continue.
					//log.Println(err)
					continue
				}

				// Stream the bytes to an unzip function
				err = unzip(dest+job.Name, reader, res.ContentLength)
				if err != nil {
					return err
				}

			}
		}
	}

	return nil
}

func unzip(dst string, reader *bytes.Reader, len int64) error {
	archive, err := zip.NewReader(reader, len)
	if err != nil {
		return err
	}

	for _, f := range archive.File {
		filePath := filepath.Join(dst, f.Name)
		log.Println("    unzipping file ", filePath)

		if !strings.HasPrefix(filePath, filepath.Clean(dst)+string(os.PathSeparator)) {
			log.Println("    invalid file path")
			return nil
		}
		if f.FileInfo().IsDir() {
			log.Println("    creating directory...")
			err := os.MkdirAll(filePath, os.ModePerm)
			if err != nil {
				return err
			}

			continue
		}

		if err := os.MkdirAll(filepath.Dir(filePath), os.ModePerm); err != nil {
			return err
		}

		dstFile, err := os.OpenFile(filePath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
		defer dstFile.Close()
		if err != nil {
			return err
		}

		fileInArchive, err := f.Open()
		defer fileInArchive.Close()
		if err != nil {
			return err
		}

		if _, err := io.Copy(dstFile, fileInArchive); err != nil {
			return err
		}
	}

	return nil
}
